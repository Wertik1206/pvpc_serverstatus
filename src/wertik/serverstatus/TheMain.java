package wertik.serverstatus;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class TheMain extends JavaPlugin implements CommandExecutor {

    public String online = "";
    public String offline = "";

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (args.length != 2) {
            if (args[0].equalsIgnoreCase("reload")) {
                loadConfig();
                sender.sendMessage("Configuration reloaded.");
            }
            else
                sender.sendMessage("./serverstatus <servername> offline/online");
        }
        if (args[1].equalsIgnoreCase("online"))
            setOnline(args[0]);
        else if (args[1].equalsIgnoreCase("offline"))
            setOffline(args[0]);
        else
            sender.sendMessage("./serverstatus <servername> offline/online");

        return true;
    }

    @Override
    public void onEnable() {

        ConsoleCommandSender console = getServer().getConsoleSender();

        getServer().getPluginCommand("serverstatus").setExecutor(this);

        if (!this.getServer().getPluginManager().getPlugin("PlaceholderAPI").isEnabled()) {
            console.sendMessage("§cDisabling due to PlaceholderAPI dependency not found.");
            this.getPluginLoader().disablePlugin(this);
        }

        new PlaceholderHook(this).hook();

        console.sendMessage("§bPlaceholderAPI successfuly hooked.");

        File configfile = new File(this.getDataFolder() + "/config.yml");
        FileConfiguration config = getConfig();

        if (!configfile.exists()) {

            console.sendMessage("§aGenerating default config.yml");

            config.set("online", "&aOnline");
            config.set("offline", "&cOffline");

            config.options().copyDefaults(true);

            saveConfig();
        }
        loadConfig();
    }

    // Set the state
    public void setOnline(String servername) {
        getConfig().set("status." + servername, true);
        saveConfig();
    }

    public void setOffline(String servername) {
        getConfig().set("status." + servername, false);
        saveConfig();
    }

    // get status
    public boolean getStatus(String servername) {
        return getConfig().getBoolean("status." + servername);
    }

    // config reload
    public void loadConfig() {
        reloadConfig();
        online = getConfig().getString("online");
        offline = getConfig().getString("offline");
    }

}
