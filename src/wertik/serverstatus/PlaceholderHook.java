package wertik.serverstatus;

import me.clip.placeholderapi.external.EZPlaceholderHook;
import org.bukkit.entity.Player;

public class PlaceholderHook extends EZPlaceholderHook {

    private TheMain plugin;

    public PlaceholderHook(TheMain plugin) {
        super(plugin, "serverstatus");
        this.plugin = plugin;
    }

    @Override
    public String onPlaceholderRequest(Player p, String params) {
        if (params.contains("status_")) {

            String name = params.replace("status_", "");

            if (plugin.getStatus(name))
                return "online";
            else
                return "offline";

        } else if (params.contains("statuscolored_")) {
            String name = params.replace("statuscolored_", "");

            if (plugin.getStatus(name))
                return plugin.getConfig().getString("online");
            else
                return plugin.getConfig().getString("offline");
        }

        return null;
    }
}
